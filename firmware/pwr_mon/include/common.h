#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>

enum {
  channel_voltage,
  channel_current,
};

typedef uint8_t channel_t;
typedef uint16_t value_t;

#endif /* __COMMON_H__ */
