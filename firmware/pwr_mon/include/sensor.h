#ifndef __SENSOR_H__
#define __SENSOR_H__

#include "common.h"

void sensor_init(void);
value_t sensor_get(channel_t channel);

#endif /* __SENSOR_H__ */
