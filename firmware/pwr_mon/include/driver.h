#ifndef __DRIVER_H__
#define __DRIVER_H__

#include "common.h"

void driver_init(void);
void driver_set(channel_t channel, value_t value);

#endif /* __DRIVER_H__ */
