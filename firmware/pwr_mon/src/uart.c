#include "macro.h"
#include "config.h"

#include <avr/io.h>
#include "uart.h"

#define _UART_soft 1
#define _UART_hard 2
#define _UART_tim0 3

#define UART_TYPE _CAT2(_UART_, _NTH0(uart_config))

#if UART_TYPE == _UART_soft
#include <util/delay.h>
#endif

#if UART_TYPE == _UART_tim0
#define USE_TIM0
#endif

#define BAUD_RATE _NTH1(uart_config)

#ifdef uart_tx_pad
#define HAS_TX
#define TX_MASK _BV(_NTH1(uart_tx_pad))
#define TX_PORT _CAT2(PORT, _NTH0(uart_tx_pad))
#define TX_DDR _CAT2(DDR, _NTH0(uart_tx_pad))

#define tx_out() (TX_DDR |= TX_MASK)
#define tx_off() (TX_DDR &= ~TX_MASK)
#define tx_set() (TX_PORT |= TX_MASK)
#define tx_res() (TX_PORT &= ~TX_MASK)
#endif

#ifdef uart_rx_pad
#define HAS_RX
#define RX_MASK _BV(_NTH1(uart_rx_pad))
#define RX_PIN _CAT2(PIN, _NTH0(uart_rx_pad))
#define RX_PORT _CAT2(PORT, _NTH0(uart_rx_pad))
#define RX_DDR _CAT2(DDR, _NTH0(uart_rx_pad))
#define rx_vect PCINT0_vect

#define rx_pup() (RX_PORT |= RX_MASK)
#define rx_off() (RX_PORT &= ~RX_MASK)
#define rx_get() (RX_PIN & RX_MASK)
#endif

void uart_init(void) {
#ifdef USE_TIM0
  TCCR0A = _BV(WGM01);	// compare mode
  TCCR0B = _BV(CS00);		// prescaler 1
#ifdef HAS_TX
  OCR0A = (F_CPU / BAUD_RATE) - 1; /* Set baudrate for tx */
#endif
#ifdef HAS_RX
  OCR0B = (F_CPU / BAUD_RATE / 4) - 1; /* Set baudrate for rx */
#endif
#endif

#ifdef HAS_TX
  tx_out();
  tx_set();
#endif

#ifdef HAS_RX
  rx_pup();
#endif
}

#ifdef HAS_TX
static inline void tx_wait(uint8_t reserved_cycles) {
#ifdef USE_TIM0
  (void)reserved_cycles;
  for (; !(TIFR0 & _BV(OCF0A)); );
  TIFR0 |= _BV(OCF0A);
#else
  _delay_us(1e6 / BAUD_RATE - 1e6 * reserved_cycles / F_CPU);
#endif
}

/* bitbanged UART transmit byte */
static void tx_byte(uint8_t data) {
  uint8_t i;

#ifdef USE_TIM0
  TCCR0B = 0;
  TCNT0 = 0;
  TIFR0 |= _BV(OCF0A);
  TCCR0B |= _BV(CS00);
  TIFR0 |= _BV(OCF0A);
#endif
  
  tx_res();
  tx_wait(2);
  
  for (i = 0; i < 8; i++) {
    if (data & 1) {
      tx_set();
    } else {
      tx_res();
    }
    data >>= 1;
    tx_wait(10);
  }
  
  tx_set();
  tx_wait(6);
}

void uart_send(const uint8_t *ptr, uint8_t len) {
  const uint8_t *end = ptr + len;
  for (; ptr < end; tx_byte(*ptr++));
}
#endif /* HAS_TX */

#ifdef HAS_RX

#define RX_IMPL 1

enum {
  rx_err = 1 << 1,
};

#define rx_dbg() (TX_PORT ^= TX_MASK) //tx_res(); tx_set()

#if RX_IMPL == 0

static inline void rx_wait(uint8_t reserved_cycles) {
#ifdef USE_TIM0
  (void)reserved_cycles;
  for (; !(TIFR0 & _BV(OCF0A)); );
  TIFR0 |= _BV(OCF0A);
#else
  _delay_us(1e6 / BAUD_RATE / 8 - 1e6 * reserved_cycles / F_CPU);
#endif
}

static inline void rx_wait0(void) {
#ifdef USE_TIM0
  (void)reserved_cycles;
  for (; !(TIFR0 & _BV(OCF0A)); );
  TIFR0 |= _BV(OCF0A);
#else
  _delay_us(1e6 / BAUD_RATE / 8);
#endif
}

static uint8_t rx_bit(void) {
  uint8_t bit = 0;
  uint8_t i = 0;
  rx_wait0();
  for (; i < 3; i++) {
    rx_wait(8);
    bit |= rx_get() ? 1 : 0;
    bit <<= 1;
  }
  rx_dbg();
  rx_wait0();
  rx_wait0();
  rx_wait0();
  if (bit == 0) { /* bit is 0 */
    rx_wait(3 + 2 + 11);
    return 0 << 7;
  }
  if (bit == 0xe) { /* bit is 1 */
    rx_wait(7 + 2 + 11);
    return 1 << 7;
  }
  return rx_err; /* line error */
}

static uint8_t rx_byte(uint8_t *data) {
  uint8_t bit;
  /* start condition */
  bit = rx_bit();
  if (bit == rx_err) {
    return uart_error;
  }
  if (bit != 0) {
    return uart_idle;
  }
  /* data receiving */
  uint8_t i = 0;
  for (; i < 8; i++) {
    bit = rx_bit();
    if (bit == rx_err) {
      return uart_error;
    }
    *data |= bit;
    *data >>= 1;
  }
  /* stop condition */
  bit = rx_bit();
  if (bit != 1) {
    return uart_error;
  }
  return uart_ok;
}

#endif

#if RX_IMPL == 1

static inline void rx_wait(uint8_t reserved_cycles) {
#ifdef USE_TIM0
  (void)reserved_cycles;
  for (; !(TIFR0 & _BV(OCF0A)); );
  TIFR0 |= _BV(OCF0A);
#else
  _delay_us(1e6 / BAUD_RATE / 2 - 1e6 * reserved_cycles / F_CPU);
#endif
}

static uint8_t rx_bit(void) {
  rx_wait(16);
  //rx_dbg();
  if (rx_get()) {
    rx_wait(4 + 11);
    return 1 << 7;
  }
  rx_wait(5 + 11);
  return 0;
}

static uint8_t rx_byte(uint8_t *data) {
  /* start condition */
  if (rx_bit() != 0) {
    return uart_idle;
  }
  /* data receiving */
  uint8_t i = 0;
  for (; i < 7; i++) {
    *data |= rx_bit();
    *data >>= 1;
  }
  *data |= rx_bit();
  /* stop condition */
  if (rx_bit() == 0) {
    return uart_error;
  }
  return uart_ok;
}

#endif

uint8_t uart_recv(uint8_t *ptr, uint8_t *len) {
  uint8_t res;
  uint8_t lim = *len;
  for (*len = 0; *len < lim && (res = rx_byte(&ptr[(*len)++])) == uart_ok; );
  return res;
}

#endif /* HAS_RX */
