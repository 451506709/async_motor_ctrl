#include "macro.h"
#include "config.h"

#ifndef RESEARCH
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#else
#include <stdio.h>
#endif /* <RESEARCH */

#include "driver.h"

#ifndef DRIVER_BITS
#define DRIVER_BITS 8
#endif

#ifdef driver_config

#ifndef RESEARCH
#  define DRIVER_TIMER _NTH0(driver_config)

#  define DRIVER_TCCRA _CAT3(TCCR, DRIVER_TIMER, A)
#  define DRIVER_TCCRB _CAT3(TCCR, DRIVER_TIMER, B)
#  define DRIVER_TCNT _CAT2(TCNT, DRIVER_TIMER)
#  define DRIVER_TCCRA_COM _BV(_CAT3(COM, DRIVER_TIMER, A1))
#  define DRIVER_TCCRB_COM _BV(_CAT3(COM, DRIVER_TIMER, B1))

#  ifdef current_output
#    define CURRENT_PORT _CAT2(PORT, _NTH0(current_output))
#    define CURRENT_DDR _CAT2(DDR, _NTH0(current_output))
#    define CURRENT_PAD _BV(_NTH1(current_output))
#    define CURRENT_OCR _CAT3(OCR, DRIVER_TIMER, _NTH2(current_output))
#  endif

#  ifdef voltage_output
#    define VOLTAGE_PORT _CAT2(PORT, _NTH0(voltage_output))
#    define VOLTAGE_DDR _CAT2(DDR, _NTH0(voltage_output))
#    define VOLTAGE_PAD _BV(_NTH1(voltage_output))
#    define VOLTAGE_OCR _CAT3(OCR, DRIVER_TIMER, _NTH2(voltage_output))
#  endif

void driver_init(void) {
  /*
#if defined(CURRENT_PORT) && defined(VOLTAGE_PORT) && (CURRENT_PORT == VOLTAGE_PORT)
  CURRENT_DDR |= CURRENT_PAD | VOLTAGE_PAD;
#else
  */
#  ifdef CURRENT_PORT
  CURRENT_DDR |= CURRENT_PAD;
#  endif
#  ifdef VOLTAGE_PORT
  VOLTAGE_DDR |= VOLTAGE_PAD;
#  endif
  /*
#endif
  */
  
  //DRIVER_TCNT = 0; /* Reset counter */
  
  DRIVER_TCCRA = 0
    | DRIVER_TCCRA_COM /* Clear OC0A on Compare Match, set OC0A at TOP */
    | DRIVER_TCCRB_COM /* Clear OC0B on Compare Match, set OC0B at TOP */
    | _BV(WGM01) | _BV(WGM00) /* Select Fast PWM mode */
    ;
  DRIVER_TCCRB = 0
    | _BV(CS00) /* Enable counter without prescaler */
    ;
}
#else /* RESEARCH */
void driver_init(void) {}
#endif /* <RESEARCH */

#define PRECISION_BITS 16

static uint8_t val2ocr(uint32_t val, uint32_t mul) {
  return val * mul >> PRECISION_BITS;
}

static inline uint32_t valmul(double top, double max) {
  return ((uint32_t)1 << (DRIVER_BITS + PRECISION_BITS)) * (SENSOR_TOP) / (top * max);
}

#define OCR_VAL(key, val) val2ocr(val, valmul(_CAT2(key, _TOP), _CAT3(DRIVER_, key, _TOP)))

#ifndef RESEARCH
#define set_value(key, val) _CAT2(key, _OCR) = OCR_VAL(key, val)
#else /* RESEARCH */
#define set_value(key, val) printf("driver_set(" #key ", %u): OCR = %u (mul:%u)\n", val, OCR_VAL(key, val), valmul(_CAT2(key, _TOP), _CAT3(DRIVER_, key, _TOP)))
#endif /* <RESEARCH */

void driver_set(channel_t channel, value_t value) {
  switch (channel) {
  case channel_current:
    set_value(CURRENT, value);
    break;
  case channel_voltage:
    set_value(VOLTAGE, value);
    break;
  }
}

#endif /* <driver_config */
