#include "macro.h"
#include "config.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "pwrmon-def.h"
#include "uart.h"
#include "sensor.h"
#include "driver.h"

#define _CS_int 1
#define _CS_pcint 2

#ifdef cs_config

#if _CAT2(_CS_, _NTH0(cs_config)) == _CS_int
#define CS_INT _BV(_CAT2(INT, _NTH1(cs_config)))
#define cs_vect _CAT3(INT, _NTH1(cs_config), _vect)
#endif

#if _CAT2(_CS_, _NTH0(cs_config)) == _CS_pcint
#define CS_PCINT _BV(_CAT2(PCINT, _NTH1(cs_config)))
#define cs_vect PCINT0_vect
#endif

#define CS_ON _NTH2(cs_config)

#define CS_MASK _BV(_NTH1(cs_input))
#define CS_PIN _CAT2(PIN, _NTH0(cs_input))
#define CS_PORT _CAT2(PORT, _NTH0(cs_input))
#define CS_DDR _CAT2(DDR, _NTH0(cs_input))

#define cs_on() (!(CS_PIN & CS_MASK) == !CS_ON)

static inline void cs_init(void) {
  //CS_PORT |= CS_MASK; /* Enable pull-up resistor */
  /* The low level generates an interrupt request */
#ifdef CS_INT
  MCUCR |= _BV(ISC01); /* The falling edge generates an interrupt request */
#endif
#ifdef CS_PCINT
  GIMSK |= _BV(PCIE); /* Enable input change detection */
#endif
}

#ifdef CS_INT
#define cs_enable() (GIMSK |= CS_INT) /* Enable external interrupt */
#define cs_disable() (GIMSK &= ~CS_INT) /* Disable external interrupt */
#endif

#ifdef CS_PCINT
#define cs_enable() (PCMSK |= CS_PCINT) /* Enable external interrupt */
#define cs_disable() (PCMSK &= ~CS_PCINT) /* Disable external interrupt */
#endif

EMPTY_INTERRUPT(cs_vect);

#endif /* <cs_config */

value_t max_voltage = 0;
value_t max_current = 0;

//#define TEST_CLOCK
//#define TEST_UART

int main(void) {
#ifdef TEST_CLOCK0
  DDRB |= _BV(0);
  TCCR0A = 0
    | _BV(COM0A1) | _BV(COM0A0) /* Set OC0A on Compare Match, clear OC0A at TOP */
    | _BV(WGM01) | _BV(WGM00) /* Fast PWM */
    ;
  TCCR0B = 0
    | _BV(WGM02) /* Fast PWM */
    | _BV(CS00) /* Enable counter without prescaler */
    ;
  OCR0A = 1;
#endif

#ifdef TEST_CLOCK
  DDRB |= _BV(0);
  TCCR0A = 0
    | _BV(COM0A0) /* Toggle OC0A on Compare Match */
    | _BV(WGM01) /* CTC mode */
    ;
  TCCR0B = 0
    | _BV(CS00) /* Enable counter without prescaler */
    ;
  OCR0A = 0;

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();

  for(;;);
#endif
  
  cs_init();
  uart_init();

#ifdef TEST_UART
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();
  sei();
  
  for (;;) {
    cs_enable();
    sleep_cpu();
    cs_disable();
    
    uint8_t data[] = { 0b01010101 };
    uint8_t size = sizeof(data);
    uint8_t res = uart_recv(data, &size);

    switch (res) {
    case uart_idle:
      break;
    case uart_ok:
      uart_send(data, sizeof(data));
      break;
    case uart_error:
      //PORTB &= ~_BV(4);
      break;
    }
  }
#endif
  
  sensor_init();
  driver_init();

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();
  sei();
  
  for (;;) {
    cs_enable();
    sleep_cpu();
    cs_disable();
    
    //if (!cs_on())
    //  continue;
    
    pwrmon_packet_t pkt;
    uint8_t len = sizeof(pkt);
    uint8_t res = uart_recv(pkt, &len);
    
    if (res == uart_error) {
      continue;
    }

    uint8_t field = pwrmon_get_field(pkt);
    value_t value = pwrmon_get_value(pkt);
    
    switch (field) {
    case pwrmon_get_voltage:
      value = sensor_get(channel_voltage);
      break;
    case pwrmon_get_current:
      value = sensor_get(channel_current);
      break;
    case pwrmon_get_max_voltage:
      value = max_voltage;
      break;
    case pwrmon_get_max_current:
      value = max_current;
      break;
    case pwrmon_set_max_voltage:
      max_voltage = value;
      driver_set(channel_voltage, value);
      break;
    case pwrmon_set_max_current:
      max_current = value;
      driver_set(channel_current, value);
      break;
    }
    
    pwrmon_set_value(pkt, value);
    uart_send(pkt, sizeof(pkt));
  }
}
