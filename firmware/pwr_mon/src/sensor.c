#include "macro.h"
#include "config.h"

#ifndef RESEARCH
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#else
#include <stdio.h>
#endif /* <RESEARCH */

#include "sensor.h"

#ifndef SENSOR_BITS
#define SENSOR_BITS 10
#endif

#define BUFFER_LENGTH _NTH0(sensor_config)

#ifndef RESEARCH

#define REFERENCE_SELECT _BV(REFS0)

#ifdef current_input
#  define CURRENT_CHANNEL _NTH2(current_input)
#endif

#ifdef voltage_input
#  define VOLTAGE_CHANNEL _NTH2(voltage_input)
#endif

#define sensor_enable() (ADCSRA |= _BV(ADEN))
#define sensor_disable() (ADCSRA &= ~_BV(ADEN))

#if _NTH1(sensor_config) > 0
#  define MEDIAN_FILTER _NTH1(sensor_config)
#  if _NTH1(sensor_config) > 1
#    define AVERAGE_FILTER 1
#  endif
#else
#  if _NTH0(sensor_config) > 1
#    define AVERAGE_FILTER 1
#  endif
#endif

EMPTY_INTERRUPT(ADC_vect);

void sensor_init(void) {
#define ADC_0_D ADC0D
#define ADC_1_D ADC1D
#define ADC_2_D ADC2D
#define ADC_3_D ADC3D
  
  DIDR0 = 0 /* Disable digital inputs on analog pins to save energy */
#ifdef VOLTAGE_CHANNEL
    | _BV(_CAT3(ADC_, VOLTAGE_CHANNEL, _D))
#endif
#ifdef CURRENT_CHANNEL
    | _BV(_CAT3(ADC_, CURRENT_CHANNEL, _D))
#endif
    ;

  ACSR = 0
    | _BV(ACD) /* Disable analog comparator */
    ;

  ADCSRA = 0
    | _BV(ADIE) /* Enable conversion interrupt */
    | _BV(ADPS1) | _BV(ADPS0) /* prescaler=8 (see datasheet, p.84) */
    ;
  ADCSRB = 0;
}
#else /* RESEARCH */

#define sensor_enable()
#define sensor_disable()
#define sleep_cpu()

void sensor_init(void) {}

uint16_t adc_val = 0;
#define ADC adc_val

#endif /* <RESEARCH */

static void quick_sort(value_t a[], uint8_t n) {
  uint8_t i, j;
  value_t p, t;
  
  if (n < 2)
    return;
  
  p = a[n / 2];
  
  for (i = 0, j = n - 1;; i++, j--) {
    while (a[i] < p)
      i++;
    while (p < a[j])
      j--;
    if (i >= j)
      break;
    t = a[i];
    a[i] = a[j];
    a[j] = t;
  }
  
  quick_sort(a, i);
  quick_sort(a + i, n - i);
}

static value_t sensor_measure(void) {
  value_t res;
  
#if BUFFER_LENGTH > 1
  value_t buffer[BUFFER_LENGTH];
  value_t *ptr = &buffer[0], *end = &buffer[BUFFER_LENGTH];
  
  sensor_enable(); /* Enable analog to digital converter */

  for (; ptr < end; ) {
    sleep_cpu();
    *ptr++ = ADC/* << 2*/;
  }

  sensor_disable(); /* Disable analog to digital converter */
  
#if defined(MEDIAN_FILTER) && MEDIAN_FILTER > 0
# define cnt MEDIAN_FILTER
  ptr = buffer + cnt / 2;
# define end (buffer + cnt / 2 + cnt)
  quick_sort(buffer, BUFFER_LENGTH);
#else /* !MEDIAN_FILTER */
# define cnt BUFFER_LENGTH
  ptr = buffer;
# define end (buffer + BUFFER_LENGTH)
#endif

#if defined(AVERAGE_FILTER) && AVERAGE_FILTER > 0
  for (res = 0; ptr < end; res += *ptr++);
  res /= cnt;
#else
  res = *ptr;
#endif
  
#else /* BUFFER_LENGTH > 1 */
  sensor_enable(); /* Enable analog to digital converter */

  sleep_cpu();
  res = ADC;// << 2;

  sensor_disable(); /* Disable analog to digital converter */
#endif /* BUFFER_LENGTH > 1 */

  return res;
}

#define PRECISION_BITS 8

static uint16_t adc2val(uint32_t val, uint32_t top) {
  return val * top >> (PRECISION_BITS + SENSOR_BITS);
}

static uint32_t valmul(double top) {
  return ((uint32_t)1 << PRECISION_BITS) * top;
}

#define ADC_GET(key) adc2val(sensor_measure(), valmul(_CAT2(key, _TOP)))

#ifndef RESEARCH
#define select_channel(key) (ADMUX = REFERENCE_SELECT | _CAT2(key, _CHANNEL))
#define get_value(key) (value = ADC_GET(key))
#else /* RESEARCH */
#define select_channel(key)
#define get_value(key) printf("sensor_get(" #key "): %u => %u (top:%u)\n", ADC, ADC_GET(key), (uint32_t)_CAT2(key, _TOP))
#endif /* <RESEARCH */

value_t sensor_get(channel_t channel) {
  value_t value = 0;
  
  switch (channel) {
#ifdef current_input
  case channel_current:
    select_channel(CURRENT);
    get_value(CURRENT);
    break;
#endif
#ifdef voltage_input
  case channel_voltage:
    select_channel(VOLTAGE);
    get_value(VOLTAGE);
    break;
#endif
  }
  
  return value;
}
