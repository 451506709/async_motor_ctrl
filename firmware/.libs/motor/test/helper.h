#include <stdio.h>

#define gnuplot(plots, datas) {                   \
    FILE *g = popen("gnuplot", "w");              \
    fprintf(g, "plot " plots "\n");               \
    datas;                                        \
    fprintf(g, "pause mouse key\n");              \
    pclose(g);                                    \
  }

#define plotstr1(title)                           \
  "'-' using 1:2 with steps title '" title "', "

#define plotfun1(fun, from, to, steps) {          \
    const real_t f = real_make(from);             \
    const real_t t = real_make(to);               \
    const real_t s = real_div(real_sub(t, f),     \
                              real_make(steps));  \
    real_t a;                                     \
    for (a = f; a < t; a = real_add(a, s)) {      \
      fprintf(g, "%f %f\n", real_float(a),        \
              real_float(fun(a)));                \
    }                                             \
    fprintf(g, "e\n");                            \
  }

#define plotrun(init, step, print, steps) {         \
    unsigned i = 0;                                 \
    init;                                           \
    for (; i < steps; i++) {                        \
      step;                                         \
      print;                                        \
    }                                               \
    fprintf(g, "e\n");                              \
  }

#define plotrun1(init, step, arg, res, steps)                      \
  plotrun(init, step,                                              \
          fprintf(g, "%f %f\n", real_float(arg), real_float(res)), \
          steps)

#define plotrun2(init, step, arg1, res1, arg2, res2, steps)          \
  plotrun(init, step,                                                \
          fprintf(g, "%f %f\n", real_float(arg1), real_float(res1)); \
          fprintf(g, "%f %f\n", real_float(arg2), real_float(res2)), \
          steps)

#define addxgrid(fmt, arg...)                                 \
  fprintf(g, "set xtics " fmt "; set grid xtics;\n", ##arg);

#define addxgrid1(incr)      \
  addxgrid("%f", real_float(incr))

#define addxgrid2(start, incr)      \
  addxgrid("%f, %f", real_float(start), real_float(incr));
