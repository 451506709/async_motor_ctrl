#include "svm.h"
#include "psc.h"
#include "helper.h"

int main(void) {
  mf_t mf;
  
  mf_init(&mf, real_make(PWM_FREQ));
  mf_set(&mf, real_make(MFR_FREQ));
  
  psc_t psc;
  psc_set(&psc, real_make(PWM_FREQ), real_make(CRIT_TIME));

  fprintf(stderr, "period=%f step=%f\n", real_float(mf.period), real_float(mf.step));

  idx2_t cpi;
  svm_t svm;
  pwm3_t psd;
  pwm3_t pwm;

#define plotpwm(var)                                         \
  plotrun1(char b = 1; svm_init(&svm); psc_init(&psd),       \
           b = !b; svm_step(&mf, &svm, &pwm);                \
           psc_step(&psc, &psd, &pwm, b & 1 ? NULL : &cpi),  \
           real_mul(mf.step, real_make(i)),                  \
           var, 100)

#define plotpwmT(var) plotpwm(real_add(pwm.var, real_make(1.5)))
#define plotpwmU(var1, var2) plotpwm(pwm.var2 - pwm.var1)

#define plotpwmM(var, num)                                              \
  plotrun1(char b = 1; svm_init(&svm);                                  \
           fprintf(g, "%f %f\n", REAL_ZERO,                             \
                   1.5 - 0.15 * num),                                   \
           b = !b; svm_step(&mf, &svm, &pwm),                           \
           real_mul(mf.step, b & 1 ?                                    \
                    real_add(real_make(i), pwm.var) :                   \
                    real_sub(real_make(i+1), pwm.var)),                 \
           real_make(!b * 0.1 + 1.5 - 0.15 * num), 100)

#define plotpwmC(var, num)                                              \
  plotrun1(char b = 1; svm_init(&svm); psc_init(&psd);                  \
           fprintf(g, "%f %f\n", REAL_ZERO,                             \
                   1.5 - 0.15 * num),                                   \
           b = !b; svm_step(&mf, &svm, &pwm);                           \
           psc_step(&psc, &psd, &pwm, b & 1 ? NULL : &cpi),             \
           real_mul(mf.step, b & 1 ?                                    \
                    real_add(real_make(i), pwm.var) :                   \
                    real_sub(real_make(i+1), pwm.var)),                 \
           real_make(!b * 0.08 + 1.5 - 0.15 * num), 100)
    
  gnuplot(
          plotstr1("Ta")
          plotstr1("Tb")
          plotstr1("Tc")
          
          plotstr1("Tab")
          plotstr1("Tbc")
          plotstr1("Tca")

          plotstr1("Ua")
          plotstr1("Ub")
          plotstr1("Uc")
          
          plotstr1("Pa")
          plotstr1("Pb")
          plotstr1("Pc"),
          
          plotpwmT(Ta)
          plotpwmT(Tb)
          plotpwmT(Tc)
          
          plotpwmU(Ta, Tb)
          plotpwmU(Tb, Tc)
          plotpwmU(Tc, Ta)
          
          plotpwmM(Ta, 1)
          plotpwmM(Tb, 2)
          plotpwmM(Tc, 3)
          
          plotpwmC(Ta, 1)
          plotpwmC(Tb, 2)
          plotpwmC(Tc, 3)

          addxgrid1(mf.step)
          );
    
  return 0;
}
