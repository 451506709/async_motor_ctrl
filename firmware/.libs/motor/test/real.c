#include "real.h"
#include "helper.h"

int main(void) {
  printf("sin(0): %f\n"
         "sin(PI/2): %f\n"
         "sin(-PI/2): %f\n"
         "sin(PI/3): %f\n"
         "sin(PI*2): %f\n"
         "sin(PI-PI/6): %f\n"
         "sin(-PI/6): %f\n"
         "sin(-PI+PI/6): %f\n",
         real_float(_real_sin(real_make(0))),
         real_float(_real_sin(real_make(M_PI/2))),
         real_float(_real_sin(real_make(-M_PI/2))),
         real_float(_real_sin(real_make(M_PI/3))),
         real_float(_real_sin(real_make(M_PI*2))),
         real_float(_real_sin(real_make(M_PI-M_PI/6))),
         real_float(_real_sin(real_make(-M_PI/6))),
         real_float(_real_sin(real_make(-M_PI+M_PI/6))));

  gnuplot(plotstr1("sin")
          plotstr1("cos"),
          plotfun1(real_sin, -3*M_PI, 3*M_PI, 500)
          plotfun1(real_cos, -3*M_PI, 3*M_PI, 500));
  
  return 0;
}
