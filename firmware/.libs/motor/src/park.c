#include "park.h"

void direct_park(const dq_t *dqs, real_t phy, dq_t *dqr) {
  real_t sin_phy = real_sin(phy);
  real_t cos_phy = real_cos(phy);
  
  /* dr = ds * cos(phy) + qs * sin(phy) */
  dqr->d = real_add(real_mul(dqs->d, cos_phy),
                    real_mul(dqs->q, sin_phy));
  
  /* qr = qs * cos(phy) - ds * sin(phy) */
  dqr->q = real_sub(real_mul(dqs->q, cos_phy),
                    real_mul(dqs->d, sin_phy));
}

void invert_park(const dq_t *dqr, real_t phy, dq_t *dqs) {
  real_t sin_phy = real_sin(phy);
  real_t cos_phy = real_cos(phy);
  
  /* ds = dr * cos(phy) - qr * sin(phy) */
  dqs->d = real_sub(real_mul(dqr->d, cos_phy),
                    real_mul(dqr->q, sin_phy));
  
  /* qs = qr * cos(phy) + dr * sin(phy) */
  dqs->q = real_add(real_mul(dqr->q, cos_phy),
                    real_mul(dqr->d, sin_phy));
}
