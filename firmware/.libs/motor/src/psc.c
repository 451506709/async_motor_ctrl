#include "motor.h"
#include "psc.h"

void psc_set(psc_t *c, real_t f, real_t t) {
	motor_assert(c != NULL, "Config pointer is NULL");
	
	c->delay = real_mul(t, f);
}

/*
	The inverter model:

	DC+ ----o-------o-------o
          |       |       |
          I        \       \
          |       |       |
       A (+)   B (-)   C (-)
          |       |       |
           \      I       I
          |       |       |
  DC- ----o-------o-------o

	The current table:

	| Sa | Sb | Sc | Idc |
	|----|----|----|-----|
	| +  | -  | -  | +Ia |
	| -  | +  | +  | -Ia |
	|    |    |    |     |
	| -  | +  | -  | +Ib |
	| +  | -  | +  | -Ib |
	|    |    |    |     |
	| -  | -  | +  | +Ic |
	| +  | +  | -  | -Ic |
	|    |    |    |     |
	| -  | -  | -  | 0   |
	| +  | +  | +  | 0   |
	
 */

void psc_step(const psc_t *c, pwm3_t *s, pwm3_t *p, idx2_t *i) {
	motor_assert(c != NULL, "Config pointer is NULL");
	motor_assert(s != NULL, "State pointer is NULL");
	motor_assert(p != NULL, "Duty-cycle values pointer is NULL");
	
	uint8_t k = 0;
	
	for (; k < 3; k++) {
		real_t *Tp = &p->T[k];
		real_t *dTp = &s->T[k];
		
		if (real_gt(*dTp, REAL_ZERO) ||
				real_lt(*dTp, REAL_ZERO)) {
			motor_debug("postadj phase=%u delta=%f\n", k, *dTp);	
		}
		
		*Tp = real_add(*Tp, *dTp);
		if (real_lt(*Tp, REAL_ZERO)) {
			*dTp = *Tp;
			*Tp = REAL_ZERO;
		} else if (real_gt(*Tp, REAL_ONE)) {
			*dTp = real_sub(*Tp, REAL_ONE);
			*Tp = REAL_ONE;
		} else {
			*dTp = REAL_ZERO;
		}
	}
	
	if (i == NULL) {
		return;
	}
	
	/*
		We may measure two phase currents:
		+I[max]
		-I[min]
		
		The time intervals for measurements:
		p->T[max] - p->T[mid]
		p->T[mid] - p->T[min]
	*/
#define max i->P1
	uint8_t mid;
#define min i->P2
	
	if (real_lt(p->Ta, p->Tb)) { /* Ta < Tb */
		if (real_lt(p->Ta, p->Tc)) { /* Ta < Tb,Tc */
			min = PWM3_A;
			if (real_lt(p->Tb, p->Tc)) { /* Ta < Tb < Tc */
				mid = PWM3_B;
				max = PWM3_C;
			} else { /* Ta < Tc < Tb */
				mid = PWM3_C;
				max = PWM3_B;
			}
		} else { /* Tc < Ta < Tb */
			min = PWM3_C;
			mid = PWM3_A;
			max = PWM3_B;
		}
	} else { /* Tb < Ta */
		if (real_lt(p->Tb, p->Tc)) { /* Tb < Ta,Tc */
			min = PWM3_B;
			if (real_lt(p->Ta, p->Tc)) { /* Tb < Ta < Tc */
				mid = PWM3_A;
				max = PWM3_C;
			} else { /* Tb < Tc < Ta */
				mid = PWM3_C;
				max = PWM3_A;
			}
		} else { /* Tc < Tb < Ta */
			min = PWM3_C;
			mid = PWM3_B;
			max = PWM3_A;
		}
	}

	/*
       :   ________:________   :
		A  :__|        :        |__:
   max :  .   _____:_____      :
		B  :_____|     :     |_____:
   mid :  .  .   __:__         :
		C  :________|  :  |________:
   min :  .  .  .  :           :
	         T1 T2
	*/
	
	real_t T1 = real_sub(p->T[max], p->T[mid]);
	real_t T2 = real_sub(p->T[mid], p->T[min]);
	
	motor_debug("Tcrit=%f T1=%f T2=%f\n", real_float(c->delay), real_float(T1), real_float(T2));
	
	real_t dT;
	
	if (real_lt(T1, c->delay)) {
		dT = real_sub(c->delay, T1);
	} else if (real_lt(T2, c->delay)) {
		dT = real_sub(T2, c->delay);
	} else {
		return;
	}
	
	real_t *Tp = &p->T[mid];
	real_t *dTp = &s->T[mid];
	
	*Tp = real_sub(*Tp, dT);
	*dTp = real_add(*dTp, dT);
	
	motor_debug("preadj T%u phase=%u delta=%f\n", (real_lt(T1, c->delay) ? 1 : 2), mid, dT);
}
