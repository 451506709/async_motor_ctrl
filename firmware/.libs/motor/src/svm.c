#include "svm.h"

void svm_init(svm_t *s) {
	s->angle = REAL_ZERO;
	s->sector = 0;
}

void svm_step(const mf_t *i, svm_t *s, pwm3_t *o) {
	/* dx = sin(60 - angle) */
	real_t dx = real_sin(real_sub(REAL_THIRD_PI, s->angle));
	/* dy = sin(angle) */
	real_t dy = real_sin(s->angle);
	
	static const uint8_t v[][3] = {
		{PWM3_A, PWM3_B, PWM3_C}, /* S0: a,b,c dx */
		{PWM3_B, PWM3_A, PWM3_C}, /* S1: b,a,c dy */
		{PWM3_B, PWM3_C, PWM3_A}, /* S2: b,c,a dx */
		{PWM3_C, PWM3_B, PWM3_A}, /* S3: c,b,a dy */
		{PWM3_C, PWM3_A, PWM3_B}, /* S4: c,a,b dx */
		{PWM3_A, PWM3_C, PWM3_B}, /* S5: a,c,b dy */
	};
	
	const uint8_t *p = v[s->sector];
	o->T[p[0]] = real_mul(REAL_HALF, real_sub(real_sub(REAL_ONE, dx), dy));
	o->T[p[1]] = real_add(o->T[p[0]], s->sector & 1 ? dy : dx);
	o->T[p[2]] = real_sub(REAL_ONE, o->T[p[0]]);
	
	s->angle = real_add(s->angle, i->step);

	if (real_ge(s->angle, REAL_THIRD_PI)) {
		s->angle = real_sub(s->angle, REAL_THIRD_PI);
		if (s->sector < 5) {
			s->sector ++;
		} else {
			s->sector = 0;
		}
	}
}
