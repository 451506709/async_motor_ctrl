#include "clarke.h"

void direct_clarke(const ab_t *ab, dq_t *dq) {
  /* d = a */
  dq->d = ab->a;
  
  /* q = (a + 2 * b) / sqrt(3) */
  dq->q = real_mul(real_add(ab->a, real_mul(ab->b, REAL_TWO)), REAL_INV_SQRT_THREE);
}

void invert_clarke(const dq_t *dq, abc_t *abc) {
  /* a = d */
  abc->a = dq->d;
  
  real_t sqrt3_q = real_mul(dq->q, REAL_SQRT_THREE);
  
  /* b = (sqrt(3) * q - d) / 2 */
  abc->b = real_mul(real_sub(sqrt3_q, dq->d), REAL_HALF);
  
  /* c = (-sqrt(3) * q - d) / 2 */
  abc->c = real_mul(real_sub(real_neg(sqrt3_q), dq->d), REAL_HALF);
}
