#ifndef __SWM_H__
#define __SWM_H__
/**
 * @defgroup swm Sine-wave 3-phase modulation
 * @brief This part implements sine-wave 3-phase modulation
 *
 * @{
 */

#include "motor.h"

/**
 * @brief The internal state of modulator
 */
typedef struct {
  /**
   * @brief The actual magnetic field angle
   */
  real_t angle;
} swm_t;

/**
 * @brief Initialize modulator state
 *
 * @param[out] s The pointer to state
 */
void swm_init(swm_t *s);

/**
 * @brief Run calculation of duty cycle values
 *
 * You must call this function synchronous with control frequency.
 *
 * @param[in] i The pointer to modulator input
 * @param[in,out] s The pointer to modulator state
 * @param[out] o The pointer to modulator output
 */
void swm_step(const mf_t *i, swm_t *s, pwm3_t *o);

/**
 * @}
 */
#endif /* __SWM_H__ */
