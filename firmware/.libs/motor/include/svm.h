#ifndef __SVM_H__
#define __SVM_H__
/**
 * @defgroup svm Space-vector 3-phase modulation
 * @brief This part implements classic space-vector 3-phase modulation.
 *
 * @{
 */

#include "motor.h"

/**
 * @brief The internal state of modulator
 */
typedef struct {
  /**
   * @brief The actual magnetic field angle into sector
   */
  real_t angle;
  /**
   * @brief The actual sector number
   */
  uint8_t sector;
} svm_t;

/**
 * @brief Initialize modulator state
 *
 * @param[out] s The pointer to state
 */
void svm_init(svm_t *s);

/**
 * @brief Run calculation of duty cycle values
 *
 * You must call this function synchronous with control frequency.
 *
 * @param[in] i The pointer to modulator input
 * @param[in,out] s The pointer to modulator state
 * @param[out] o The pointer to modulator output
 */
void svm_step(const mf_t *i, svm_t *s, pwm3_t *o);

/**
 * @}
 */
#endif /* __SVM_H__ */
