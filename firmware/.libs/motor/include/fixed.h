#include <stdint.h>

typedef int32_t fixed;
typedef int64_t dfixed;

#ifndef FP_FRAC
#define FP_FRAC 16
#endif

#define fp_float(a) ((a) / (float)(1<<(FP_FRAC)))
#define fp_make(a) ((fixed)((a) * (1<<(FP_FRAC))))
#define fp_add(a, b) ((a) + (b))
#define fp_sub(a, b) ((a) - (b))
#define fp_mul(a, b) ((fixed)(((dfixed)(a) * (b)) >> (FP_FRAC)))
#define fp_div(a, b) ((fixed)(((dfixed)(a) << (FP_FRAC)) / (b)))
