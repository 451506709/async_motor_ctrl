#ifndef __PSC_H__
#define __PSC_H__
/**
 * @defgroup psc 3-phase phase-shift correction
 * @brief This part implements phase-shift correction (compensation).
 * 
 * Phase-shift PWM correction can help to improve final wave-form for phases current measurement using single current sensor.
 *
 * @{
 */

#include "motor.h"

/**
 * @brief The corrector config
 */
typedef struct {
  /**
   * @brief The minimal relative time for compensation
   */
  real_t delay;
} psc_t;

/**
 * @brief Initialize corrector state
 *
 * @param[out] s The pointer to state
 */
static inline void psc_init(pwm3_t *s) {
  pwm3_init(s);
}

/**
 * @brief Configure corrector delay
 *
 * @param[out] s The pointer to state
 * @param[in] f The PWM frequency
 * @param[in] t The critical time to adjust to
 */
void psc_set(psc_t *c, real_t f, real_t t);

/**
 * @brief Run correction of duty cycle values
 *
 * @param[in] c The pointer to config
 * @param[in,out] s The corrector state
 * @param[in,out] p The pointer to pwm values
 * @param[out] i The pointer to phase indexes
 *
 * The pointer to phase indexes can be NULL.
 * In which case the correction won't be performed,
 * but the effect of previous correction will be applied in any case.
 */
void psc_step(const psc_t *c, pwm3_t *s, pwm3_t *p, idx2_t *i);

/**
 * @}
 */
#endif /* __PSC_H__ */
