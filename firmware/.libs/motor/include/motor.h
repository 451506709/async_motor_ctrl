#ifndef __MOTOR_H__
#define __MOTOR_H__
/**
 * @defgroup motor Basic types and operations
 * @brief This part implements the basic types and operations for motor control
 *
 * @{
 */

#include <stddef.h>
#include <stdint.h>
#include "real.h"

#ifdef MOTOR_ASSERT
#include <assert.h>
#define motor_assert(exp, fmt, ...) assert(exp)
#else
#define motor_assert(...)
#endif

#ifdef MOTOR_DEBUG
#include <stdio.h>
#define motor_debug(fmt, ...) printf("[MOTOR DEBUG]: " fmt, ##__VA_ARGS__)
#else /* MOTOR_DEBUG */
#define motor_debug(...)
#endif /* MOTOR_DEBUG */

/**
 * @brief The magnitude-frequency modulator data
 */
typedef struct {
  /**
   * @brief The period of modulation signal calculations (steps)
   *
   * Usually equals to PWM period.
   */
  real_t period;
  /**
   * @brief The step of angle
   *
   * The value is dependent from control frequency and field revolution speed.
   */
  real_t step;
} mf_t;

/**
 * @brief Set control step frequency
 *
 * Usually you must call this method once under initialization procedure.
 *
 * @param[out] i The pointer to modulator data
 * @param[in] f The control step frequency in Hz
 */
void mf_init(mf_t *i, real_t f);

/**
 * @brief Set magnetic field revolution frequency
 *
 * You must call this method for changing current field revolution frequency i.e. to accelerate and decelerate motor.
 * You must call it after setting step frequency.
 *
 * @param[in,out] i The pointer to modulator data
 * @param[in] f The magnetic field frequency in Hz
 */
void mf_set(mf_t *i, real_t f);

/**
 * @brief The @p a-b vector data
 */
typedef struct {
  /**
   * @brief The @p a value
   */
  real_t a;
  /**
   * @brief The @p b value
   */
  real_t b;
} ab_t;

/**
 * @brief The @p a-b-c vector data
 */
typedef struct {
  /**
   * @brief The @p a value
   */
  real_t a;
  /**
   * @brief The @p b value
   */
  real_t b;
  /**
   * @brief The @p c value
   */
  real_t c;
} abc_t;

/**
 * @brief Use @p a-b-c vector as @p a-b vector
 *
 * @param[in] abc The pointer to @p a-b-c vector values
 * @return ab The same pointer to @p a-b vector values
 */
static inline ab_t *as_ab(abc_t *abc) {
  return (ab_t*)abc;
}

/**
 * @brief The @p d-q vector data
 */
typedef struct {
  /**
   * @brief The @p d value
   */
  real_t d;
  /**
   * @brief The @p q value
   */
  real_t q;
} dq_t;

/**
 * @brief 3-phase PWM channel indexes
 */
enum {
  /**
   * @brief Channel A
   */
  PWM3_A = 0,
  /**
   * @brief Channel B
   */
  PWM3_B = 1,
  /**
   * @brief Channel C
   */
  PWM3_C = 2,
};

/**
 * @brief 3-phase PWM duty-cycle values
 *
 * The relative time values which provided by modulator and used to driving 3-phase pulse-width modulation unit.
 */
typedef union {
  /**
   * @brief The named phase values fiedls
   */
  struct {
    /**
     * @brief Phase A value
     */
    real_t Ta;
    /**
     * @brief Phase B value
     */
    real_t Tb;
    /**
     * @brief Phase C value
     */
    real_t Tc;
  };
  /**
   * @brief The array of phase values
   */
  real_t T[3];
} pwm3_t;

/**
 * @brief Initialize PWM duty-cycle values
 *
 * @param[out] s The pointer to PWM duty-cycle values
 */
void pwm3_init(pwm3_t *s);

/**
 * @brief Index pair values
 *
 * This can be used to store phase indexes, for example for current measurement purpose.
 */
typedef union {
  /**
   * @brief The named phase index fiedls
   */
  struct {
    /**
     * @brief Phase a index
     */
    uint8_t a;
    /**
     * @brief Phase b index
     */
    uint8_t b;
  };
  /**
   * @brief The array of phase indexes
   */
  uint8_t i[2];
  /**
   * @brief The named phase index fiedls
   */
  struct {
    /**
     * @brief Phase 1 index
     */
    uint8_t P1;
    /**
     * @brief Phase 2 index
     */
    uint8_t P2;
  };
  /**
   * @brief The array of phase indexes
   */
  uint8_t P[2];
} idx2_t;

/**
 * @}
 */
#endif /* __MOTOR_H__ */
