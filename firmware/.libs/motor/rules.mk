libmotor.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libmotor
libmotor.INHERIT := firmware libqfp
libmotor.CDIRS := $(libmotor.BASEPATH)include
libmotor.SRCS := $(addprefix $(libmotor.BASEPATH)src/,\
  motor.c \
  clarke.c \
  park.c \
  swm.c \
  svm.c \
  psc.c \
  real.c)

libmotor.LTGS = trig:$(trig_lookup.size)

TARGET.OPTS += libmotor
libmotor.OPTS := $(libmotor.BASEPATH)src/motor.cf
