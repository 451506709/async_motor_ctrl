#include "evt.h"

#ifndef EVT_BUFFER
#define EVT_BUFFER 1
#endif

#if EVT_BUFFER == 1

static evt_t evt = evt_none;

evt_t evt_pull(void) {
  evt_t e = evt;
  evt = evt_none;
  return e;
}

void evt_push(evt_t e) {
  evt = e;
}

#else /* EVT_BUFFER > 1 */

static uint8_t cnt = 0;
static uint8_t pull_idx = 0;
static uint8_t push_idx = 0;
static evt_t buf[EVT_BUFFER] = {
  [0 ... (EVT_BUFFER-1)] = evt_none
};

evt_t evt_pull(void) {
  if (cnt == 0) {
    return evt_none;
  }

  cnt --;
  evt_t e = buf[pull_idx ++];

  if (pull_idx == EVT_BUFFER) {
    pull_idx = 0;
  }
  
  return e;
}

void evt_push(evt_t e) {
  if (cnt == EVT_BUFFER) {
    return;
  }

  cnt ++;
  buf[push_idx ++] = e;
  
  if (push_idx == EVT_BUFFER) {
    push_idx = 0;
  }
}

#endif
