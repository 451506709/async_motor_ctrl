#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>

#include "macro.h"
#include "config.h"

#include "enc.h"
#include "evt.h"
#include "ticks.h"

#ifdef instance
#define ENC_NUM instance
#else
#define ENC_NUM
#endif

enc_def(ENC_NUM);

#define ENC_CFG _CAT3(enc, ENC_NUM, _config)

#define ENCA_CFG _UNWR(_NTH0(ENC_CFG))

#define ENCA_PORT _CAT2(GPIO, _NTH0(ENCA_CFG))
#define ENCA_PAD _CAT2(GPIO, _NTH1(ENCA_CFG))
#define ENCA_EXTI _CAT2(EXTI, _NTH1(ENCA_CFG))

#define ENCA_ACT _NTH2(ENCA_CFG) /* active state */

#define ENCA_IRQ _CAT3(NVIC_EXTI, _NTH1(ENCA_CFG), _IRQ)
#define enca_isr _CAT3(exti, _NTH1(ENCA_CFG), _isr)

#define ENCB_CFG _UNWR(_NTH1(ENC_CFG))

#define ENCB_PORT _CAT2(GPIO, _NTH0(ENCB_CFG))
#define ENCB_PAD _CAT2(GPIO, _NTH1(ENCB_CFG))
#define ENCB_EXTI _CAT2(EXTI, _NTH1(ENCB_CFG))

#define ENCB_ACT _NTH2(ENCB_CFG) /* active state */

#define ENCB_IRQ _CAT3(NVIC_EXTI, _NTH1(ENCB_CFG), _IRQ)
#define encb_isr _CAT3(exti, _NTH1(ENCB_CFG), _isr)

#define CW_EVT _NTH2(ENC_CFG) /* cw event */
#define CCW_EVT _NTH3(ENC_CFG) /* ccw event */

static int8_t state_; /* old state */
static int8_t count = 0;

void enc_fn(ENC_NUM, init)(void) {
  nvic_enable_irq(ENCA_IRQ);
  nvic_enable_irq(ENCB_IRQ);
  
#ifdef STM32F1
#if ENCA_PORT == ENCB_PORT
  gpio_set_mode(ENCA_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                ENCA_PAD | ENCB_PAD);
#else /* !(ENCA_PORT == ENCB_PORT) */
  gpio_set_mode(ENCA_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                ENCA_PAD);
  gpio_set_mode(ENCB_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                ENCB_PAD);
#endif /* <(ENCA_PORT == ENCB_PORT) */
  /* pull-up */
#if ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT
#if ENCA_ACT
  gpio_clear(ENCA_PORT, ENCA_PAD | ENCB_PAD);
#else
  gpio_set(ENCA_PORT, ENCA_PAD | ENCB_PAD);
#endif
#else /* !(ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT) */
#if ENCA_ACT
  gpio_clear(ENCA_PORT, ENCA_PAD);
#else
  gpio_set(ENCA_PORT, ENCA_PAD);
#endif
#if ENCB_ACT
  gpio_clear(ENCB_PORT, ENCB_PAD);
#else
  gpio_set(ENCB_PORT, ENCB_PAD);
#endif
#endif /* <(ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT) */
#else /* !STM32F1 */
#if ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT
  gpio_mode_setup(ENCA_PORT, GPIO_MODE_INPUT,
                  ENCA_ACT ? GPIO_PUPD_PULLDOWN :
                  GPIO_PUPD_PULLUP, ENCA_PAD | ENCB_PAD);
#else /* !(ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT) */
  gpio_mode_setup(ENCA_PORT, GPIO_MODE_INPUT,
                  ENCA_ACT ? GPIO_PUPD_PULLDOWN :
                  GPIO_PUPD_PULLUP, ENCA_PAD);
  gpio_mode_setup(ENCB_PORT, GPIO_MODE_INPUT,
                  ENCB_ACT ? GPIO_PUPD_PULLDOWN :
                  GPIO_PUPD_PULLUP, ENCB_PAD);
#endif /* <(ENCA_PORT == ENCB_PORT && ENCA_ACT == ENCB_ACT) */
#endif /* <STM32F1 */

  exti_select_source(ENCA_EXTI, ENCA_PORT);
  //exti_set_trigger(ENCA_EXTI, ENCA_ACT ? EXTI_TRIGGER_RISING : EXTI_TRIGGER_FALLING);
  exti_set_trigger(ENCA_EXTI, EXTI_TRIGGER_BOTH);

  exti_select_source(ENCB_EXTI, ENCB_PORT);
  //exti_set_trigger(ENCB_EXTI, ENCB_ACT ? EXTI_TRIGGER_RISING : EXTI_TRIGGER_FALLING);
  exti_set_trigger(ENCB_EXTI, EXTI_TRIGGER_BOTH);
  
  state_ =
    (!gpio_get(ENCA_PORT, ENCA_PAD) == !ENCA_ACT ? (1 << 1) : 0) |
    (!gpio_get(ENCB_PORT, ENCB_PAD) == !ENCB_ACT ? (1 << 0) : 0);

  exti_enable_request(ENCA_EXTI | ENCB_EXTI);
}

void enc_fn(ENC_NUM, done)(void) {
  exti_disable_request(ENCA_EXTI | ENCB_EXTI);

#ifdef STM32F1
#if ENCA_PORT == ENCB_PORT
  gpio_set_mode(ENCA_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                ENCA_PAD | ENCB_PAD);
#else /* !(ENCA_PORT == ENCB_PORT) */
  gpio_set_mode(ENCA_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                ENCA_PAD);
  gpio_set_mode(ENCB_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                ENCB_PAD);
#endif /* <(ENCA_PORT == ENCB_PORT) */
#else /* !STM32F1 */
#if ENCA_PORT == ENCB_PORT
  gpio_mode_setup(ENCA_PORT, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, ENCA_PAD | ENCB_PAD);
#else /* !(ENCA_PORT == ENCB_PORT) */
  gpio_mode_setup(ENCA_PORT, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, ENCA_PAD);
  gpio_mode_setup(ENCB_PORT, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, ENCB_PAD);
#endif /* <(ENCA_PORT == ENCB_PORT) */
#endif /* <STM32F1 */

  nvic_disable_irq(ENCA_IRQ);
  nvic_disable_irq(ENCB_IRQ);
}

#ifdef ENC_DBG
uint8_t enc_seq[256];
uint8_t *enc_ptr = enc_seq;
#endif

#ifndef ENC_USE_TOGGLE
#define ENC_USE_TOGGLE 0
#endif

static void enc_trig(
#if ENC_USE_TOGGLE
                     int8_t toggle
#else
                     void
#endif
                     ) {
#if ENC_USE_TOGGLE
  int8_t state = state_ ^ toggle;
#ifdef ENC_DBG
  //*enc_ptr++ = toggle;
#endif
#else
  uint8_t state =
    (!gpio_get(ENCA_PORT, ENCA_PAD) == !ENCA_ACT ? (1 << 1) : 0) |
    (!gpio_get(ENCB_PORT, ENCB_PAD) == !ENCB_ACT ? (1 << 0) : 0);
#endif

#define trans(old, new) (((old) << 2) | (new))

  switch (trans(state_, state)) {
  case trans(1, 0): /* decrement */
  case trans(3, 1): /* (counter clockwise) */
  case trans(2, 3):
  case trans(0, 2):
    count --;
    break;
  case trans(0, 1): /* increment */
  case trans(1, 3): /* (clockwise) */
  case trans(3, 2):
  case trans(2, 0):
    count ++;
    break;
  default:
    return;
  }

#ifdef ENC_DBG
  *enc_ptr++ = state;
#endif
  
  state_ = state;
  
  switch (count) {
  case -4:
    evt_push(CCW_EVT);
    break;
  case 4:
    evt_push(CW_EVT);
    break;
  default:
    return;
  }

  count = 0;
}

void enca_isr(void) {
  exti_reset_request(ENCA_EXTI);
#if ENC_USE_TOGGLE
  enc_trig(1 << 1);
#else
  enc_trig();
#endif
}

void encb_isr(void) {
  exti_reset_request(ENCB_EXTI);
#if ENC_USE_TOGGLE
  enc_trig(1 << 0);
#else
  enc_trig();
#endif
}
