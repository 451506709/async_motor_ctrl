#ifndef __BTN_H__
#define __BTN_H__ "btn.h"

#include "evt.h"

#define btn_fn(N, F) _CAT4(btn, N, _, F)

#define btn_def(N)            \
  void btn_fn(N, init)(void); \
  void btn_fn(N, done)(void)

#endif /* __BTN_H__ */
