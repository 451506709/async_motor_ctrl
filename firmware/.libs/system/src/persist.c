#include <libopencm3/stm32/flash.h>

#include "persist.h"

extern uint8_t _param_start;
extern uint8_t _param_end;

#define cookie 0xaabb

static uint32_t seek_end(uint8_t size) {
  uint32_t addr = (uint32_t)&_param_start;
  
  for (; addr < (uint32_t)&_param_end - size && 
         MMIO16(addr) == cookie;
       addr += size);
  
  return addr;
}

bool persist_load_real(uint8_t *ptr, uint8_t len) {
  uint16_t size = 2 + len;

  if (size & 0x1) {
    size ++;
  }
  
  uint32_t addr = seek_end(size);
  
  if (addr == (uint32_t)&_param_start) {
    return false;
  }
  
  addr -= size;
  
  if (MMIO16(addr) != cookie) {
    return false;
  }
  
  uint32_t end = addr + size;
  
  addr += 2;

  if (len & 0x1) {
    end -= 2;
  }
  
  for (; addr < end; addr += 2, ptr += 2) {
    *(uint16_t*)ptr = MMIO16(addr);
  }
  
  if (len & 0x1) {
    *(uint8_t*)ptr = MMIO8(addr);
  }
  
  return true;
}

bool persist_save_real(const uint8_t *ptr, uint8_t len) {
  uint16_t size = 2 + len;

  if (size & 0x1) {
    size ++;
  }
  
  uint32_t addr = seek_end(size);

  if (addr > (uint32_t)&_param_start) {
    /* need compare with last saved value before save new */
    const uint8_t *_ptr = ptr;
    uint32_t _addr = addr;
    
    _addr -= size;
  
    if (MMIO16(_addr) != cookie) {
      return false;
    }

    uint32_t end = _addr + size;
    
    _addr += 2;
    
    if (len & 0x1) {
      end -= 2;
    }
    
    for (; _addr < end; _addr += 2, _ptr += 2) {
      if (*(uint16_t*)_ptr != MMIO16(_addr)) {
        goto save;
      }
    }
    
    if (len & 0x1) {
      if (*(uint8_t*)_ptr != MMIO8(_addr)) {
        goto save;
      }
    }
    
    return true;
  }
 save:
  
  flash_unlock();

  if (addr > (uint32_t)&_param_end + size) {
    flash_erase_page((uint32_t)&_param_start);
    addr = (uint32_t)&_param_start;
  }

  uint32_t end = addr + size;
  
  flash_program_half_word(addr, cookie);
  addr += 2;
  
  if (len & 0x1) {
    end -= 2;
  }
  
  for (; addr < end; addr += 2, ptr += 2) {
    flash_program_half_word(addr, *(const uint16_t*)ptr);
  }
  
  if (len & 0x1) {
    flash_program_half_word(addr, *(const uint8_t*)ptr);
  }
  
  flash_lock();

  return true;
}
