/**
 * @file memtool.h
 * @brief Memory consumption measurement tool
 *
 * @section Memory tool
 */
#ifndef MEMTOOL_H
#define MEMTOOL_H "memtool.h"

#include <stddef.h>

#ifndef MEM_PREFILL
/**
 * @brief The memory prefill value
 *
 * The RAM will be filled using this value to determine real memory usage.
 * By default we use 0x55, but you can simply redefine (for ex. -DMEM_PREFILL=0xaa)
 */
#define MEM_PREFILL 0x55
#endif

#ifndef MEM_FREELEN
/**
 * @brief The free memory length (bytes)
 *
 * The number of bytes, which filled using @p MEM_PREFILL, to determine unused memory.
 * By default we use 8 bytes, but you can simply redefine (for ex. -DMEM_FREELEN=4)
 */
#define MEM_FREELEN 8
#endif

#if MEM_FREELEN < 1
#error "The MEM_FREELEN will be greater than 0."
#endif

#if MEM_FREELEN < 4
#warning "The MEM_FREELEN too low."
#endif

#if MEM_FREELEN > 32
#warning "The MEM_FREELEN too high."
#endif

/**
 * @brief The memory info
 */
typedef struct {
  /**
   * @brief Max used heap (bytes)
   *
   * The maximum size of heap memory, which been used by application.
   */
  int heap_usage;
  /**
   * @brief Max used stack (bytes)
   *
   * The maximum size of stack, which been required by application.
   */
  int stack_usage;
} mem_info_t;

/**
 * @brief Fill memory using pattern
 *
 * You need call this function before init code of your application to prepare memory.
 * The currently unused memory will be filled using @p MEM_PREFILL value.
 */
void mem_prefill(void);

/**
 * @brief Measure actual memory usage
 *
 * You may use this function to get real used memory in any reasonable place.
 * Usually you need call this after several full runs of mainloop of application.
 * The free memory detection uses @MEM_FREELEN to determine unused memory.
 * Maybe you need to  this value if detection inaccurate.
 *
 * @param res The pointer to result
 */
void mem_measure(mem_info_t *res);

#endif /* MEMTOOL_H */
