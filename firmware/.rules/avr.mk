avr.CDEFS += $(if $(avr.frequency),F_CPU=$(avr.frequency),$(error avr.frequency not defined))
avr.COPTS += pack-struct short-enums unsigned-bitfields unsigned-char

attiny.INHERIT := avr

attiny13.INHERIT := attiny
attiny13.ROM ?= 1024
attiny13.RAM ?= 64
attiny13.MCU ?= attiny13
attiny13.CMACH ?= mcu=attiny13
