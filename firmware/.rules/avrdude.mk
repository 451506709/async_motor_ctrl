OCD_INTERFACE ?= avrdoper
OCD_TRANSPORT ?= stk500hvsp
OCD_TARGET ?= attiny13
AVR_FUSES ?= lfuse hfuse lock

OCD_COMMAND = $(Q)avrdude \
	$(if $(OCD_INTERFACE),-P $(OCD_INTERFACE)) \
	$(if $(OCD_TRANSPORT),-c $(OCD_TRANSPORT)) \
	$(if $(OCD_TARGET),-p $(OCD_TARGET))

define FLASH_RULES
flash.img.$(1): $$($(1).BIN)
	$(OCD_COMMAND) -U "flash:w:$$<:a" \
	  $$(foreach n,$(AVR_FUSES),\
	  $$(if $$($(1).$$(n)),-U '$$(n):w:$$($(1).$$(n)):m'))
endef

read.fuse:
	$(OCD_COMMAND) $(foreach n,$(AVR_FUSES),-U '$(n):r:-:h')

terminal:
	$(OCD_COMMAND) -t
