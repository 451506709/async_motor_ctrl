define _LDS_PRESET # $(1) - base name, $(2) - sub name, $(3) - ROM size, $(4) - RAM size
$(1)$(2).INHERIT := $(1)
$(1)$(2).ROM.SIZE := $(3)
$(1)$(2).RAM.SIZE := $(4)
endef

LDS_PRESET = $(eval $(call _LDS_PRESET,$(1),$(2),$(3),$(4)))

# Path patterns
LDS_P = ld/$(1).ld

define LDSOPTS_EXPAND
$$(foreach parent,$$($(1).INHERIT),$$(eval $$(call LDSOPTS_EXPAND,$$(parent))))

# include
$$(call INHERITS,$(1),ADD,INCLUDE)

# ROM
$$(call INHERITS,$(1),SET,ROM.ADDR)
$$(call INHERITS,$(1),SET,ROM.SIZE)

# RAM
$$(call INHERITS,$(1),SET,RAM.ADDR)
$$(call INHERITS,$(1),SET,RAM.SIZE)

# bootloader
$$(call INHERITS,$(1),SET,BOOTLD.SIZE)

# parameters
$$(call INHERITS,$(1),SET,PARAMS.SIZE)

endef

# Loader script
define LDS_RULES
$$(eval $$(call LDSOPTS_EXPAND,$(1)))

$(1).LDSCRIPT := $$(call LDS_P,$(1))

$$($(1).LDSCRIPT):
	@echo TARGET $(1) LD SCRIPT
	$(Q)mkdir -p $$(dir $$@)
	$(Q)echo '/* generated by ldgen.mk */' >>$$@
	$(Q)echo '' >>$$@
	$(Q)echo 'MEMORY {' >$$@
ifneq (,$$($(1).BOOTLD.SIZE))
	$(Q)echo '  ldr (rx) : ORIGIN = $$($(1).ROM.ADDR), LENGTH = $$($(1).BOOTLD.SIZE) /* bootloader */' >>$$@
endif
	$(Q)echo '  rom (rx) : ORIGIN = $$($(1).ROM.ADDR)$$(if $$($(1).BOOTLD.SIZE),+$$($(1).BOOTLD.SIZE)), LENGTH = $$($(1).ROM.SIZE)$$(if $$($(1).BOOTLD.SIZE),-$$($(1).BOOTLD.SIZE))$$(if $$($(1).PARAMS.SIZE),-$$($(1).PARAMS.SIZE)) /* application */' >>$$@
ifneq (,$$($(1).PARAMS.SIZE))
	$(Q)echo '  par (r) : ORIGIN = $$($(1).ROM.ADDR)+$$($(1).ROM.SIZE)-$$($(1).PARAMS.SIZE), LENGTH = $$($(1).PARAMS.SIZE) /* parameters */' >>$$@
endif
	$(Q)echo '  ram (rwx) : ORIGIN = $$($(1).RAM.ADDR), LENGTH = $$($(1).RAM.SIZE)' >>$$@
	$(Q)echo '}' >>$$@
	$(Q)echo '' >>$$@
ifneq (,$$($(1).PARAMS.SIZE))
	$(Q)echo '/* parameters storage symbols */' >>$$@
	$(Q)echo 'PROVIDE(_param_start = ORIGIN(par));' >>$$@
	$(Q)echo 'PROVIDE(_param_end = ORIGIN(par) + LENGTH(par));' >>$$@
endif
	$(Q)echo '' >>$$@
	$(Q)echo '/* include ld scripts */' >>$$@
	$(Q)$$(foreach lds,$$($(1).INCLUDE),echo 'INCLUDE $$(lds)' >>$$@;)

clean: clean.lds.$(1)
clean.lds.$(1):
	@echo TARGET $(1) CLEAN
	$(Q)rm -f $$($(1).LDSCRIPT)

endef
