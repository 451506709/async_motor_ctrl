EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:bridge
LIBS:addon
LIBS:stm32
LIBS:hall
LIBS:driver
LIBS:lph7366
LIBS:handler
LIBS:iface
LIBS:radio
LIBS:esdprot
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "3-Phase Inverter"
Date "2017-02-07"
Rev "Rev. 0.3"
Comp "Illumium.Org"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6000 1100 2300 1400
U 58600832
F0 "Control" 60
F1 "control.sch" 60
$EndSheet
$Sheet
S 6000 2900 2300 1400
U 5772CE06
F0 "Driver" 60
F1 "driver.sch" 60
$EndSheet
$Sheet
S 2800 2900 2300 1400
U 5774CF4B
F0 "Power" 60
F1 "power.sch" 60
$EndSheet
$Comp
L CONN_01X04 H1
U 1 1 577AC382
P 5600 6950
F 0 "H1" H 5600 7200 50  0000 C CNN
F 1 "PCB_BOX" V 5700 6950 50  0000 C CNN
F 2 "Local:ATX_BOX" H 5600 6950 50  0001 C CNN
F 3 "" H 5600 6950 50  0000 C CNN
	1    5600 6950
	1    0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 577AC403
P 5300 7200
F 0 "#PWR01" H 5300 6950 50  0001 C CNN
F 1 "GND" H 5300 7050 50  0000 C CNN
F 2 "" H 5300 7200 50  0000 C CNN
F 3 "" H 5300 7200 50  0000 C CNN
	1    5300 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 6800 5300 6800
Wire Wire Line
	5300 6800 5300 7200
Wire Wire Line
	5300 6900 5400 6900
Connection ~ 5300 6900
$Sheet
S 2800 1100 2300 1400
U 5860F761
F0 "Supply" 60
F1 "supply.sch" 60
$EndSheet
NoConn ~ 5400 7000
NoConn ~ 5400 7100
$EndSCHEMATC
