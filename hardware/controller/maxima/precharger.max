/* C - current limiter capacitor */
/* R - current limiting resistor */
/* P[R] - power of limiting resistor */
/* C[P] - power capacitor */
/* Z - full resistance */
/* U - powering voltage */
/* T[C] - the time of full charge */
/* I[C] - average charge current */
/* f - supply frequency */

EQ[X[C]]: X[C] = 1 / (2 * %pi * f * C);
EQ[Z]: Z = R + X[C], EQ[X[C]];
EQ[T[C]]: T[C] = 3 * Z * C[P], EQ[Z];
EQ[I[C]]: I[C] = C[P] * U / T[C], EQ[T[C]];
EQ[P[R]]: P[R] = I[C]^2 * R, EQ[I[C]];

PR[IN]: [f=50, C[P]=600e-6*4, C=470e-9*2, R=20, U=310]$
EQ[Z], PR[IN], numer;
EQ[T[C]], PR[IN], numer;
EQ[I[C]], PR[IN], numer;
EQ[P[R]], PR[IN], numer;
