EESchema Schematic File Version 2
LIBS:spice
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V V1
U 1 1 56F2CA7B
P 8600 3400
F 0 "V1" H 8475 3600 60  0000 C CNN
F 1 "PWL 0mS 0mV 10mS 500mV" V 8850 3400 60  0000 C CNN
F 2 "" H 8275 3400 60  0000 C CNN
F 3 "" H 8275 3400 60  0000 C CNN
	1    8600 3400
	1    0    0    -1  
$EndComp
Text Notes 8000 6200 0    60   ~ 0
+pspice\n* \n.control\n  options nopage\n  options savecurrents\n  linearize\n  tran 1uS 10mS 0mS\n  display\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(VIN)\n  + v(PRE)\n  + 2.5+v(CAP2)/10\n  + 5+v(OUT)/10\n*  + v(CAP1)\n*  + v(CAP2)\n*  + v(VST)\n*  + fft(v(OUT))\n.endc
Text GLabel 8600 3700 3    60   Input ~ 0
VIN
Text Notes 9500 2500 0    39   ~ 0
-pspice\n* \n* SS9012 PNP model\n.MODEL QSS9012 PNP \n+ IS=20.417E-15 \n+ BF=143.30 \n+ VAF=47.750 \n+ IKF=.57435 \n+ ISE=75.858E-15 \n+ NE=2 \n+ BR=14.345 \n+ NR=.999 \n+ VAR=86.140 \n+ IKR=.42651 \n+ ISC=1.5849E-15 \n+ NC=1.0087 \n+ RE=20.000E-3 \n+ RB=57.500 \n+ RC=.7426 \n+ CJE=35.100E-12 \n+ VJE=.866 \n+ MJE=.411 \n+ CJC=19.100E-12 \n+ VJC=.787 \n+ MJC=.394 \n+ TF=10.000E-9 \n+ XTF=10 \n+ VTF=10 \n+ ITF=1 \n+ TR=10.000E-9 \n+ EG=1.0885 \n+ XTB=1.4132 
Text Notes 600  6600 0    39   ~ 0
-pspice\n* \n* (C) National Semiconductor, Inc.\n*LM258 DUAL OPERATIONAL AMPLIFIER MACRO-MODEL\n*\n* connections:      non-inverting input\n*                   |   inverting input\n*                   |   |   positive power supply\n*                   |   |   |   negative power supply\n*                   |   |   |   |   output\n*                   |   |   |   |   |\n*                   |   |   |   |   |\n.SUBCKT LM258    1   2  99  50  28\n*\n*Features:\n*Eliminates need for dual supplies\n*Large DC voltage gain =             100dB\n*High bandwidth =                     1MHz\n*Low input offset voltage =            2mV\n*Wide supply range =       +-1.5V to +-16V\n*\n*NOTE: Model is for single device only and simulated\n*      supply current is 1/2 of total device current.\n*      Output crossover distortion with dual supplies\n*      is not modeled.\n*\n****************INPUT STAGE**************\n*\nIOS 2 1 5N\n*^Input offset current\nR1 1 3 500K\nR2 3 2 500K\nI1 99 4 100U\nR3 5 50 517\nR4 6 50 517\nQ1 5 2 4 QX\nQ2 6 7 4 QX\n*Fp2=1.2 MHz\nC4 5 6 128.27P\n*\n***********COMMON MODE EFFECT***********\n*\nI2 99 50 75U\n*^Quiescent supply current\nEOS 7 1 POLY(1) 16 49 2E-3 1\n*Input offset voltage.^\nR8 99 49 60K\nR9 49 50 60K\n*\n*********OUTPUT VOLTAGE LIMITING********\nV2 99 8 1.63\nD1 9 8 DX\nD2 10 9 DX\nV3 10 50 .635\n*\n**************SECOND STAGE**************\n*\nEH 99 98 99 49 1\nG1 98 9 POLY(1) 5 6 0 9.8772E-4 0 .3459\n*Fp1=7.86 Hz\nR5 98 9 101.2433MEG\nC3 98 9 200P\n*\n***************POLE STAGE***************\n*\n*Fp=2 MHz\nG3 98 15 9 49 1E-6\nR12 98 15 1MEG\nC5 98 15 7.9577E-14\n*\n*********COMMON-MODE ZERO STAGE*********\n*\n*Fpcm=10 KHz\nG4 98 16 3 49 5.6234E-8               \nL2 98 17 15.9M\nR13 17 16 1K\n*\n**************OUTPUT STAGE**************\n*\nF6 50 99 POLY(1) V6 300U 1\nE1 99 23 99 15 1\nR16 24 23 17.5\nD5 26 24 DX\nV6 26 22 .63V\nR17 23 25 17.5\nD6 25 27 DX\nV7 22 27 .63V\nV5 22 21 0.27V\nD4 21 15 DX\nV4 20 22 0.27V\nD3 15 20 DX\nL3 22 28 500P\nRL3 22 28 100K\n*\n***************MODELS USED**************\n*\n.MODEL DX D(IS=1E-15)\n.MODEL QX PNP(BF=1.111E3)\n*\n.ENDS
$Comp
L V V2
U 1 1 577BC251
P 9300 3400
F 0 "V2" H 9175 3600 60  0000 C CNN
F 1 "DC 5V" V 9550 3400 60  0000 C CNN
F 2 "" H 8975 3400 60  0000 C CNN
F 3 "" H 8975 3400 60  0000 C CNN
	1    9300 3400
	1    0    0    -1  
$EndComp
Text GLabel 9300 3100 1    60   Input ~ 0
VCC
$Comp
L 0 #GND01
U 1 1 577BC25A
P 9300 3700
F 0 "#GND01" H 9300 3600 40  0001 C CNN
F 1 "0" H 9300 3630 40  0000 C CNN
F 2 "" H 9300 3700 60  0000 C CNN
F 3 "" H 9300 3700 60  0000 C CNN
	1    9300 3700
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 577BCC0C
P 4900 2400
F 0 "R1" H 4900 2500 60  0000 C CNN
F 1 "8K2" H 4900 2300 60  0000 C CNN
F 2 "" H 4900 2400 60  0000 C CNN
F 3 "" H 4900 2400 60  0000 C CNN
	1    4900 2400
	1    0    0    -1  
$EndComp
Text GLabel 3000 3000 0    60   Input ~ 0
VIN
$Comp
L 0 #GND02
U 1 1 577BBCE9
P 8600 3100
F 0 "#GND02" H 8600 3000 40  0001 C CNN
F 1 "0" V 8600 3030 40  0000 C CNN
F 2 "" H 8600 3100 60  0000 C CNN
F 3 "" H 8600 3100 60  0000 C CNN
	1    8600 3100
	-1   0    0    1   
$EndComp
$Comp
L OA X1
U 1 1 577BDD35
P 5800 1600
F 0 "X1" H 5900 1750 60  0000 C CNN
F 1 "LM258" H 6000 1450 60  0000 C CNN
F 2 "" H 5700 1600 60  0000 C CNN
F 3 "" H 5700 1600 60  0000 C CNN
	1    5800 1600
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND03
U 1 1 577BDDCE
P 5000 2100
F 0 "#GND03" H 5000 2000 40  0001 C CNN
F 1 "0" H 5000 2030 40  0000 C CNN
F 2 "" H 5000 2100 60  0000 C CNN
F 3 "" H 5000 2100 60  0000 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND04
U 1 1 577BDDF3
P 5800 2000
F 0 "#GND04" H 5800 1900 40  0001 C CNN
F 1 "0" H 5800 1930 40  0000 C CNN
F 2 "" H 5800 2000 60  0000 C CNN
F 3 "" H 5800 2000 60  0000 C CNN
	1    5800 2000
	1    0    0    -1  
$EndComp
Text GLabel 5800 1200 1    60   Input ~ 0
VCC
$Comp
L C C1
U 1 1 577BDEDB
P 5800 2400
F 0 "C1" H 5800 2525 60  0000 C CNN
F 1 "1n" H 5800 2275 60  0000 C CNN
F 2 "" H 5150 2950 60  0000 C CNN
F 3 "" H 5150 2950 60  0000 C CNN
	1    5800 2400
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 577BE179
P 7150 2400
F 0 "R2" H 7150 2500 60  0000 C CNN
F 1 "100K" H 7150 2300 60  0000 C CNN
F 2 "" H 7150 2400 60  0000 C CNN
F 3 "" H 7150 2400 60  0000 C CNN
	1    7150 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3700 8600 3600
Wire Wire Line
	9300 3100 9300 3200
Wire Wire Line
	9300 3600 9300 3700
Wire Wire Line
	4300 2200 4600 2200
Wire Wire Line
	8600 3200 8600 3100
Wire Wire Line
	5500 1700 5300 1700
Wire Wire Line
	5300 1700 5300 2600
Wire Wire Line
	5100 2400 5700 2400
Wire Wire Line
	5800 1900 5800 2000
Wire Wire Line
	5800 1200 5800 1300
Connection ~ 5300 2400
Wire Wire Line
	6300 2400 5900 2400
Wire Wire Line
	6300 1600 6100 1600
$Comp
L OA X2
U 1 1 577BE30A
P 7200 1600
F 0 "X2" H 7300 1750 60  0000 C CNN
F 1 "LM258" H 7400 1450 60  0000 C CNN
F 2 "" H 7100 1600 60  0000 C CNN
F 3 "" H 7100 1600 60  0000 C CNN
	1    7200 1600
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 577BE51A
P 6700 1200
F 0 "R3" H 6700 1300 60  0000 C CNN
F 1 "100K" H 6700 1100 60  0000 C CNN
F 2 "" H 6700 1200 60  0000 C CNN
F 3 "" H 6700 1200 60  0000 C CNN
	1    6700 1200
	0    -1   -1   0   
$EndComp
Text GLabel 7200 1200 1    60   Input ~ 0
VCC
Wire Wire Line
	7200 1200 7200 1300
Wire Wire Line
	6700 1400 6700 2500
Wire Wire Line
	7700 1600 7700 3400
Wire Wire Line
	7500 1600 7800 1600
$Comp
L 0 #GND05
U 1 1 577BE713
P 7200 2000
F 0 "#GND05" H 7200 1900 40  0001 C CNN
F 1 "0" H 7200 1930 40  0000 C CNN
F 2 "" H 7200 2000 60  0000 C CNN
F 3 "" H 7200 2000 60  0000 C CNN
	1    7200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1900 7200 2000
$Comp
L 0 #GND06
U 1 1 577BE732
P 6700 3000
F 0 "#GND06" H 6700 2900 40  0001 C CNN
F 1 "0" H 6700 2930 40  0000 C CNN
F 2 "" H 6700 3000 60  0000 C CNN
F 3 "" H 6700 3000 60  0000 C CNN
	1    6700 3000
	1    0    0    -1  
$EndComp
Connection ~ 7700 1600
Text Notes 8300 2050 0    39   ~ 0
-pspice\n* \n.SUBCKT X1N4148 1 2 \n*\n* The resistor R1 does not reflect \n* a physical device. Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 5.827E+9 \nD1 1 2 D1N4148\n*\n.MODEL D1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9 \n.ENDS
Text GLabel 7800 1600 2    60   Input ~ 0
OUT
Text GLabel 6300 2000 0    47   Input ~ 0
CAP2
Text GLabel 5300 2000 2    47   Input ~ 0
CAP1
Text GLabel 6700 2000 0    47   Input ~ 0
VST
$Comp
L R R4
U 1 1 577BFA07
P 4600 1800
F 0 "R4" H 4600 1900 60  0000 C CNN
F 1 "47K" H 4600 1700 60  0000 C CNN
F 2 "" H 4600 1800 60  0000 C CNN
F 3 "" H 4600 1800 60  0000 C CNN
	1    4600 1800
	0    -1   1    0   
$EndComp
$Comp
L R R5
U 1 1 577BFAAA
P 5000 1800
F 0 "R5" H 5000 1900 60  0000 C CNN
F 1 "47K" H 5000 1700 60  0000 C CNN
F 2 "" H 5000 1800 60  0000 C CNN
F 3 "" H 5000 1800 60  0000 C CNN
	1    5000 1800
	0    -1   1    0   
$EndComp
Wire Wire Line
	5000 2000 5000 2100
Wire Wire Line
	4600 1500 5500 1500
Wire Wire Line
	5000 1500 5000 1600
Connection ~ 5000 1500
Wire Wire Line
	6300 1600 6300 2400
Wire Wire Line
	6900 1700 6300 1700
Connection ~ 6300 1700
Wire Wire Line
	6700 1500 6900 1500
$Comp
L R R7
U 1 1 577BFF5E
P 6700 2700
F 0 "R7" H 6700 2800 60  0000 C CNN
F 1 "100K" H 6700 2600 60  0000 C CNN
F 2 "" H 6700 2700 60  0000 C CNN
F 3 "" H 6700 2700 60  0000 C CNN
	1    6700 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6700 2400 6950 2400
Connection ~ 6700 1500
Connection ~ 6700 2400
Wire Wire Line
	7700 2400 7350 2400
Wire Wire Line
	6700 900  6700 1000
Text GLabel 6700 900  1    60   Input ~ 0
VCC
Wire Wire Line
	6700 2900 6700 3000
$Comp
L QNPN Q1
U 1 1 577C03AA
P 5400 3400
F 0 "Q1" H 5350 3600 60  0000 C CNN
F 1 "QSS9013" H 5250 3200 60  0000 C CNN
F 2 "" H 5075 3400 60  0000 C CNN
F 3 "" H 5075 3400 60  0000 C CNN
	1    5400 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 3600 5300 3800
$Comp
L 0 #GND07
U 1 1 577C051B
P 5300 3800
F 0 "#GND07" H 5300 3700 40  0001 C CNN
F 1 "0" H 5300 3730 40  0000 C CNN
F 2 "" H 5300 3800 60  0000 C CNN
F 3 "" H 5300 3800 60  0000 C CNN
	1    5300 3800
	1    0    0    -1  
$EndComp
Text Notes 10350 2850 0    47   ~ 0
-pspice\n* \n.model QSS9013 npn\n+is=3.40675e-14\n+bf=166\n+vaf=67\n+ikf=1.164\n+ise=12.37f\n+ne=2\n+br=15.17\n+var=40.84 \n+ikr=0.261352\n+isc=1.905f\n+nc=1.066\n+rb=63.2\n+irb=5.62u\n+rbm=22.1\n+re=0.02\n+rc=0.7426\n+cje=3.53e-11\n+vje=0.808\n+mje=0.372\n+cjc=1.74e-11\n+vjc=0.614\n+mjc=0.388\n+xcjc=0.349\n+xtb=1.4025\n+eg=1.0999\n+xti=3\n+fc=0.5\n+Vceo=20\n+Icrating=0.5
$Comp
L R R6
U 1 1 577C09BB
P 5300 2800
F 0 "R6" H 5300 2900 60  0000 C CNN
F 1 "5K6" H 5300 2700 60  0000 C CNN
F 2 "" H 5300 2800 60  0000 C CNN
F 3 "" H 5300 2800 60  0000 C CNN
	1    5300 2800
	0    -1   1    0   
$EndComp
Wire Wire Line
	5300 3000 5300 3200
$Comp
L R R8
U 1 1 577C24C1
P 6000 3400
F 0 "R8" H 6000 3500 60  0000 C CNN
F 1 "1K" H 6000 3300 60  0000 C CNN
F 2 "" H 6000 3400 60  0000 C CNN
F 3 "" H 6000 3400 60  0000 C CNN
	1    6000 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3400 5600 3400
Wire Wire Line
	7700 3400 6200 3400
Connection ~ 7700 2400
$Comp
L OA X3
U 1 1 577CA7FE
P 4000 2200
F 0 "X3" H 4100 2350 60  0000 C CNN
F 1 "LM258" H 4200 2050 60  0000 C CNN
F 2 "" H 3900 2200 60  0000 C CNN
F 3 "" H 3900 2200 60  0000 C CNN
	1    4000 2200
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 577CACE5
P 3300 3000
F 0 "R11" H 3300 3100 60  0000 C CNN
F 1 "10K" H 3300 2900 60  0000 C CNN
F 2 "" H 3300 3000 60  0000 C CNN
F 3 "" H 3300 3000 60  0000 C CNN
	1    3300 3000
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 577CADB7
P 4000 3000
F 0 "R12" H 4000 3100 60  0000 C CNN
F 1 "56K" H 4000 2900 60  0000 C CNN
F 2 "" H 4000 3000 60  0000 C CNN
F 3 "" H 4000 3000 60  0000 C CNN
	1    4000 3000
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND08
U 1 1 577CAE16
P 4000 2600
F 0 "#GND08" H 4000 2500 40  0001 C CNN
F 1 "0" H 4000 2530 40  0000 C CNN
F 2 "" H 4000 2600 60  0000 C CNN
F 3 "" H 4000 2600 60  0000 C CNN
	1    4000 2600
	1    0    0    -1  
$EndComp
Text GLabel 4000 1800 1    60   Input ~ 0
VCC
Wire Wire Line
	4000 2500 4000 2600
Wire Wire Line
	4000 1800 4000 1900
Wire Wire Line
	3700 2300 3600 2300
Wire Wire Line
	3600 2300 3600 3000
Wire Wire Line
	3500 3000 3800 3000
Connection ~ 3600 3000
Wire Wire Line
	3100 3000 3000 3000
Wire Wire Line
	4200 3000 4400 3000
Wire Wire Line
	4400 3000 4400 2200
Connection ~ 4400 2200
$Comp
L R R10
U 1 1 577CB15E
P 3300 2400
F 0 "R10" H 3300 2500 60  0000 C CNN
F 1 "1K" H 3300 2300 60  0000 C CNN
F 2 "" H 3300 2400 60  0000 C CNN
F 3 "" H 3300 2400 60  0000 C CNN
	1    3300 2400
	0    -1   1    0   
$EndComp
$Comp
L 0 #GND09
U 1 1 577CB31C
P 3300 2700
F 0 "#GND09" H 3300 2600 40  0001 C CNN
F 1 "0" H 3300 2630 40  0000 C CNN
F 2 "" H 3300 2700 60  0000 C CNN
F 3 "" H 3300 2700 60  0000 C CNN
	1    3300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2600 3300 2700
$Comp
L R R9
U 1 1 577CB3B3
P 3300 1800
F 0 "R9" H 3300 1900 60  0000 C CNN
F 1 "39K" H 3300 1700 60  0000 C CNN
F 2 "" H 3300 1800 60  0000 C CNN
F 3 "" H 3300 1800 60  0000 C CNN
	1    3300 1800
	0    -1   1    0   
$EndComp
Wire Wire Line
	3700 2100 3300 2100
Wire Wire Line
	3300 2000 3300 2200
Connection ~ 3300 2100
Wire Wire Line
	3300 1600 3300 1500
Text GLabel 3300 1500 1    60   Input ~ 0
VCC
Wire Wire Line
	4600 1500 4600 1600
Wire Wire Line
	4600 2000 4600 2400
Text GLabel 4400 2600 2    47   Input ~ 0
PRE
Text Notes 2650 5450 0    47   ~ 0
/* Stage A: (maxima) */\nEQ[1]: U[p] = U[cc] * R[3] / (R[3] + R[4])$\nEQ[2]: U[n] = (U[out] - U[in]) * R[2] / (R[2] + R[1]) + U[in]$\nEQ[3]: EQ[2], U[n] = U[p], EQ[1]$\nEQ[4]: EQ[3], U[in]=U[in(min)], U[out]=U[out(min)]$\nEQ[5]: EQ[3], U[in]=U[in(max)], U[out]=U[out(max)]$\nEQ[6]: solve(EQ[5], U[out(max)])[1];\nEQ[7]: solve(EQ[5], R[1])[1];\nEQ[8]: solve(EQ[1], R[4])[1];\nEQ[9]: solve(EQ[4], U[out(min)])[1]$\nPR[1]: [U[in(min)]=0.0, U[in(max)]=-0.4, U[cc]=5, R[2]=10e3, R[3]=1e3]$\nEQ[8], PR[1], U[p]=0.1;\nPR[2]: [R[4]=39e3]$\nEQ[1], PR[1], PR[2];\nEQ[7], PR[1], PR[2], U[out(max)]=3.0;\nPR[3]: [R[1]=56e3]$\nEQ[6], PR[1], PR[2], PR[3];\nEQ[9], PR[1], PR[2], PR[3];
Text Notes 10100 4100 0    47   ~ 0
-pspice\n* \n.MODEL D1N4148 D \n+ IS = 4.352E-9 \n+ N = 1.906 \n+ BV = 110 \n+ IBV = 0.0001 \n+ RS = 0.6458 \n+ CJO = 7.048E-13 \n+ VJ = 0.869 \n+ M = 0.03 \n+ FC = 0.5 \n+ TT = 3.48E-9
Wire Wire Line
	4600 2400 4700 2400
Connection ~ 4600 2200
$EndSCHEMATC
